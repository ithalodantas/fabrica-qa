import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class AmazonTeste {

    private WebDriver driver;

    @Before
    public void abrir(){
       // System.setProperty() Não uso por conta que uso o chrome driver no path do sistema

        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://www.amazon.com.br/");
    }

    @After
    public void sair(){
        driver.quit();
    }

    @Test
    public void maisVendido() throws InterruptedException {
        Thread.sleep(3000);
        driver.findElement(By.id("nav-hamburger-menu")).click();
        Thread.sleep(3000);

        driver.findElement(By.xpath("//*[@id=\"hmenu-content\"]/ul[1]/li[2]/a")).click();
        Thread.sleep(3000);

        Assert.assertEquals("Mais vendidos" , driver.findElement(By.xpath("//*[@id=\"zg_banner_text\"]")).getText());

        Thread.sleep(3000);
    }

    @Test
    public void testarBuscador() throws InterruptedException {          // No caso essa seria a atividade 3
        Thread.sleep(3000);
        driver.findElement(By.xpath("//*[@id=\"twotabsearchtextbox\"]")).sendKeys("Qualquer");
        driver.findElement(By.id("nav-search-submit-button")).click();
        Thread.sleep(3000);
        Assert.assertEquals("RESULTADOS" , driver.findElement(By.xpath("//*[@id=\"search\"]/div[1]/div[1]/div/span[3]/div[2]/div[1]/div/span/div/div/span")).getText());
        Thread.sleep(3000);
    }

    @Test
    public void testarAutenticacaoSenhaIncorreta() throws InterruptedException {
        driver.findElement(By.id("nav-link-accountList")).click();
        driver.findElement(By.id("ap_email")).sendKeys("sajfm2asdm2@gmail.com");
        driver.findElement(By.id("continue")).click();
        Thread.sleep(3000);
        Assert.assertEquals("Não encontramos uma conta associada a este endereço de e-mail" , driver.findElement(By.xpath("//*[@id=\"auth-error-message-box\"]/div/div/ul/li/span")).getText());
        Thread.sleep(3000);
    }

    // Atividade 4
    @Test
    public void testeLivro() throws InterruptedException {
        driver.findElement(By.xpath("//*[@id=\"nav-xshop\"]/a[6]")).click();
        Thread.sleep(3000);
        driver.findElement(By.xpath("//*[@id=\"twotabsearchtextbox\"]")).sendKeys("Livros");
        driver.findElement(By.id("nav-search-submit-button")).click();
        Assert.assertEquals("RESULTADOS" , driver.findElement(By.xpath("//*[@id=\"search\"]/div[1]/div[1]/div/span[3]/div[2]/div[2]/div/span/div/div/span")).getText());

    }

    @Test
    public void testeNovidades() throws InterruptedException {
        driver.findElement(By.xpath("//*[@id=\"nav-hamburger-menu\"]/span")).click();
        driver.findElement(By.xpath("//*[@id=\"hmenu-content\"]/ul[1]/li[3]/a")).click();
        Thread.sleep(3000);
        Assert.assertEquals("Novidades na Amazon" , driver.findElement(By.xpath("//*[@id=\"zg_banner_text\"]")).getText());

    }

    // Atividade 5

    @Test
    public void testeOfertasDoDia(){
        driver.findElement(By.xpath("//*[@id=\"nav-xshop\"]/a[4]")).click();
        Assert.assertEquals("Ofertas e Promoções" , driver.findElement(By.xpath("//*[@id=\"slot-3\"]/div/h1")).getText());
    }

    @Test
    public void testeJogo(){
        driver.findElement(By.xpath("//*[@id=\"twotabsearchtextbox\"]")).sendKeys("Halo 5");
        driver.findElement(By.xpath("//*[@id=\"nav-search-submit-button\"]")).click();
        driver.findElement(By.xpath("//*[@id=\"search\"]/div[1]/div[1]/div/span[3]/div[2]/div[2]/div/div/div/div/div[2]/div[1]/h2/a/span")).click();
        Assert.assertEquals("Halo 5: Guardians - Xbox One" , driver.findElement(By.xpath("//*[@id=\"productTitle\"]")).getText());
    }


}
